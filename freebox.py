#! /usr/bin/env python3

import os
import sys
import json
import time
import hmac
import base64
import requests

from hashlib import sha1


FILE_DB = os.getenv("FILE_DB", "db.json")
VALIDATE_TTL = os.getenv("VALIDATE_TTL", 5)

data = {}

"""
   Helpers
"""


def ask_info():
    global data
    new_data = {}

    for key in (
        ("app_name", "Name of application"),
        ("app_id", "Single worded application id"),
        ("app_version", "Current version of the application ( format : 0.0.0 )"),
        ("device_name", "Name of the device"),
    ):
        new_data[key[0]] = input("{} : ".format(key[1]))

    print("informations == {}".format(new_data))
    confirm = input("confirm ? _/n: ")

    if confirm.lower in ("n", "no", "non"):
        sys.exit(0)

    data = {**data, **new_data}
    for key in ("app_token", "challenge", "session_id", "password_salt", "track_id"):
        data[key] = None
    data["app_state"] = "pending"


def get_url(endpoint):
    """
    Returns a string of the form
    
    'https://gdj3gk9b.fbxos.fr:922/api/v6/login'
    """
    if not endpoint.startswith("/"):
        endpoint = "/{}".format(endpoint)

    url = "http{ssl}://{api_domain}:{api_port}{api_base_uri}/v{api_version}{endpoint}".format(
        ssl="s" if data["https_port"] else "",
        api_domain=data["api_domain"],
        api_port=data["https_port"] or "",
        api_base_uri=data["api_base_uri"] or "",
        api_version=data["api_version"],
        endpoint=endpoint,
    )
    return url


def send_request(uri, verb="get", *args, **kwargs):
    global data
    verb = verb.lower()

    method = getattr(requests, verb)
    if not method:
        raise AttributeError("Verb not valid")

    payload = {}
    try:
        payload = kwargs["data"]

        payload["session_token"] = data["session_token"]
        payload["app_id"] = data["app_id"]

    except KeyError:
        pass

    res = method(uri, *args, **kwargs)
    print(res, res.json())
    res.raise_for_status()
    res = res.json()
    if res["success"] == False:
        raise Exception("Request {} failed: {}".format(uri, res["msg"]))
    print("res == {}", res)
    data["challenge"] = res["result"]["challenge"]

    return res["result"]


"""
   / Helpers
"""


"""
    Do stuff
"""


def register_app():
    """
        Register the application, both on freebox side and in data
    """
    global data
    authorize_base = "/login/authorize"

    payload = {}

    for key in ("app_id", "app_name", "app_version", "device_name"):
        payload[key] = data[key]

    authorization_request = requests.post(
        get_url(authorize_base), data=json.dumps(payload), verify=False
    )
    authorization_request.raise_for_status()
    res = authorization_request.json()
    print(res)
    try:
        data = {**data, **res["result"]}
    except KeyError:
        raise Exception(
            "\033[031mError while registering app, received :: {}\033[0m".format(res)
        )


def validate_state():
    """
        Validate that the appliation got accepted
    """
    global data

    print("\033[031m Validating...\033[0m")
    authorize_poll_base = "/login/authorize/{}".format(data["track_id"])
    url = get_url(authorize_poll_base)
    pending = True

    while data["app_state"] == "pending":
        # bwaaaaah
        paul = requests.get(url, verify=False)
        paul.raise_for_status()

        res = paul.json()
        if res["success"] == True:
            status = res["result"]["status"]

            if status != "pending":
                data["app_state"] = status
                print("Connection ended with status {}".format(status))
                data["challenge"] = res["result"]["challenge"]
                if data["app_state"] not in ("granted", "accepted"):
                    del data["track_id"]
                    data["app_token"] = None
                    sys.exit(1)
                else:
                    data = {**data, **res["result"]}
                break
        else:
            print("🥺 🥺 🥺 🥺 ")
        time.sleep(VALIDATE_TTL)


def connect():
    challenge_base = "/login"

    print("\033[031m Connecting...\033[0m")
    res = requests.get(get_url(challenge_base), verify=False).json()["result"]
    print("Challenge == %s" % res)
    for key in ("challenge", "password_salt"):
        data[key] = res[key]

    login_base = "/login/session"
    url = get_url(login_base)

    print(
        "\033[033mapp token\033[0m {}, challenge {}".format(
            data["app_token"], data["challenge"]
        )
    )

    digester = hmac.new(
        data["app_token"].encode("utf8"), data["challenge"].encode("utf8"), sha1
    )
    password = digester.hexdigest()

    payload = {
        "app_id": data["app_id"],
        "password": password,
        "app_version": data["app_version"],
    }
    print("payload == {}".format(payload))
    res = requests.post(url, json=payload, verify=False)
    print(res.json())
    # res.raise_for_status()


def ls(path, only_folder=False, count_subfolder=False, remove_hidden=False):
    """
        List a sub directory

        returns a list of files as documented by the api
        raise httpRequestException or Exception
    """
    ls_base = "/fs/ls/{}"

    path_b64 = base64.b64encode(path.encode("utf8")).decode("utf8")
    url = get_url(ls_base.format(path_b64))

    options = {
        "onlyFolder": only_folder,
        "countSubFolder": count_subfolder,
        "removeHidden": remove_hidden,
    }
    listing = send_request(url, "get", params=options, verify=False)
    return listing


"""
    / Do stuff
"""


if __name__ == "__main__":

    def main():
        if "--reset" in sys.argv:
            ask_info()
        if not data.get("track_id"):
            register_app()
        if data["app_state"] == "pending":
            validate_state()
        connect()

    """ data here is like a database used as a python global object """
    with open(FILE_DB) as f:
        data = json.load(f)

    try:
        main()
    except KeyboardInterrupt:
        print("KeyBoard Interrupt")
    finally:
        with open(FILE_DB, "w") as f:
            json.dump(data, f, indent=4)
